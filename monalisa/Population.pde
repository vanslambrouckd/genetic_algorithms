import java.util.List;
import java.util.HashSet;
import java.util.Collections;
import java.util.Comparator;

class Population {
  float mutationRate;
  DNA[] population;
  ArrayList<DNA> matingPool;
  int generations;
  PImage target;
  boolean finished;
  
  Population(float m, int num, PImage targ) {
    mutationRate = m;
    population = new DNA[num];
    target = targ;
    finished = false;
    
    //println("target", target);
    
    matingPool = new ArrayList<DNA>();
    generations = 0;
    
    for (int i = 0; i < population.length; i++) {
      population[i] = new DNA(target.width*target.height);
    }
    
    calcFitness();
  }
  
  void calcFitness() {
    for (int i = 0; i < population.length; i++) {
      population[i].fitness(target);
      //println(population[i].fitness);
    }
  }
  
  void selection() {
    calcFitness();
    
    matingPool.clear();
    
    float maxFitness = getMaxFitness();
    
    for (int i = 0; i < population.length; i++) {
      float fitnessNormal = population[i].getFitness();
      
      /*
      println("maxFitness", maxFitness);
      
      */
      println("fitnessNormal", fitnessNormal);
      /*
      float fitnessNormal = map(fitness, 0, maxFitness, 0, 100);
      */
      //float fitnessNormal = (100/maxFitness)*population[i].getFitness();
      //int n = (int) (fitnessNormal * 100);  // Arbitrary multiplier
      int n = (int)fitnessNormal;
      //println("n", n);
      //println("bright", population[i].genes[0].bright);
      /*
      println("maxFitness", maxFitness);
      println("target", target);
      println("bright", population[i].getDNA().genes[0]);
      println("fitness", population[i].getFitness());
      println("fitnessNormal", fitnessNormal);
      println("n", n);
      println("---------------------");
      */
      for (int j = 0; j < n; j++) {
        matingPool.add(population[i]);
      }
    }
    
    List<DNA> newList = new ArrayList<DNA>(new HashSet<DNA>(matingPool));
    Collections.sort(newList, new Comparator<DNA>() 
    {
         @Override
         public int compare(DNA lhs, DNA rhs) {
    
           return Integer.valueOf(lhs.genes[0].bright).compareTo(rhs.genes[0].bright);
          }
     });
    
    String unique = "";
    for (DNA item: newList) {
      unique += "," + item.genes[0].bright;
    }
    
    //println(unique);
    
    //println(matingPool.size());
  }
  
  // Making the next generation
  void reproduction() {
    // Refill the population with children from the mating pool
    for (int i = 0; i < population.length; i++) {
      // Sping the wheel of fortune to pick two parents
      
      int m = int(random(matingPool.size()));
      int d = int(random(matingPool.size()));
      
      // Pick two parents
      DNA momgenes = matingPool.get(m);
      DNA dadgenes = matingPool.get(d);
      
      // Mate their genes
      DNA childgenes = momgenes.crossover(dadgenes);
     
      // Mutate their genes
      childgenes.mutate(mutationRate);
      if (childgenes.fitness(target) < momgenes.fitness(target)) {
        childgenes = momgenes;
      }
      // Fill the new population with the new child
      population[i] = childgenes;
    }
    generations++;
    
  }
  
  // Find highest fintess for the population
  float getMaxFitness() {
    float record = 0;
    for (int i = 0; i < population.length; i++) {
      //println("fitnesss", population[i].fitness);
      if (population[i].fitness(target) > record) {
        record = population[i].getFitness();
      }
    }
    return record;
  }
  
  DNA getBest() {
    float record = 0;
    int index = 0;
    for (int i = 0; i < population.length; i++) {
      //println("fitnesss", population[i].fitness);
      if (population[i].fitness > record) {
        record = population[i].fitness;
        index = i;
      }
    }
    
    if (record >= 95) {
      finished = true;
    }
    /*
    target.loadPixels();
    int targetBrightness = (int)brightness(target.pixels[index]);
    if (population[index].dna.genes[0] == targetBrightness) {
      println(population[index].dna.genes[0], targetBrightness);
      finished = true;
    }
    */
    return population[index];
  }
  
  boolean finished() {
    return finished;
  }
  
  int getGenerations() {
    return generations;
  }
  
  float getAverageFitness() {
    float total = 0;
    for (int i = 0; i < population.length; i++) {
      total += population[i].fitness;
    }
    return total / (population.length);
  }
  
  float getPopulationSize() {
    return population.length;
  }
}