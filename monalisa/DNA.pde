class DNA {
  Col[] genes;
  float fitness;
  
  DNA(int num_genes) {
    genes = new Col[num_genes];
    for (int i = 0; i < genes.length; i++) {
      genes[i] = new Col((int)random(0,100));
    }
  }
  
  DNA(Col[] newgenes) {
    genes = newgenes;
  }
  
  DNA crossover(DNA partner) {
    /*
    //POGING 1:
    int crossover = int(random(genes.length));
    for (int i = 0; i < genes.length; i++) {
      
      if (i > crossover) child[i] = genes[i];
      else               child[i] = partner.genes[i];
    }
    DNA newgenes = new DNA(child);
    return newgenes;
    */
    
    /*
    //POGING 2:
    int[] child = new int[genes.length];
    
    for (int i = 0; i < genes.length; i++) {
      child[i] = (genes[i] + partner.genes[i])/2;
    }
    DNA newgenes = new DNA(child);
    return newgenes;
    */
    
    
    //POGING 3:
    Col[] childgenes = new Col[genes.length];
    
    int crossover = int(random(genes.length));
    for (int i = 0; i < genes.length; i++) {
      if (i > crossover) childgenes[i] = genes[i];
      else               childgenes[i] = partner.genes[i];
    }
    
    DNA newgenes = new DNA(childgenes);
    return newgenes;
  }
  
  float fitness(PImage target) {
    target.loadPixels();
    
    float totalFitness = 0;
    float pixelFitness = 0;
    for (int i = 0; i < genes.length; i++) {
      pixelFitness = genes[i].fitness(target, i);
      //println("pixelfitness", pixelFitness);
      totalFitness += pixelFitness;
    }
    
    fitness = totalFitness = totalFitness/genes.length;
    //println("totalFitness", totalFitness);
    return totalFitness;
  }
  
  void mutate(float m) {
    
    for (int i = 0; i < genes.length; i++) {
      float prob = random(1);
      if (prob < m) {
         genes[i] = new Col((int)random(0,100));
         //genes[i] = (int)random(100);
         //genes[i] = (int)map(genes[i], -genes[i], genes[i], 0, 100);
         //genes[i] = Math.abs(genes[i]);
      }
    }
  }
  
  float getFitness() {
    return fitness;
  }
}