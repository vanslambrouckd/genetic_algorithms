class Col {
  int bright;
  float fitness;
  
  Col(int brightness) {
    bright = brightness;
    fitness = 0;
  }
  
  float fitness(PImage target, int pixelPos) {
    target.loadPixels();
    int targetBrightness = (int)brightness(target.pixels[pixelPos]);
    
    //test 3:
    fitness = Math.abs(targetBrightness-bright);
    //fitness = pow(fitness, 2);
    fitness = map(fitness, 0, 100, 100, 0);
    //println("fitness", fitness);
    
    //fitness = pow(target-bright, 2);
    
    /*
    println("target", target);
    println("bright", bright);
    println("fitness", fitness);
    println("----------------------");
    */
    
    return fitness;
  }
  
  float getFitness() {
    return fitness;
  }
}