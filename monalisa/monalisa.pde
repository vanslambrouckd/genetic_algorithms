PFont f;
PImage sourceImg;
PImage resultImg;

Population population;
float mutationRate;
int populationSize;

void setup() {
  colorMode(HSB, 360, 100, 100);
  
  size(800,200);
  f = createFont("Courier", 32, true);
  
  sourceImg = loadImage("ml4.bmp");
  
  sourceImg.loadPixels();
  for (int i = 0; i < sourceImg.width*sourceImg.height; i++) {
    //println(brightness(sourceImg.pixels[i]));
    sourceImg.pixels[i] = color (0, 0, brightness(sourceImg.pixels[i])); //color(grey);
    //println(brightness(sourceImg.pixels[i]));
  }
  sourceImg.updatePixels();
  
  resultImg = createImage(sourceImg.width, sourceImg.height, HSB);
  resultImg.loadPixels();
 
  mutationRate = 0.1;
  populationSize = 100;
  println("populationSize: "+populationSize);
  population = new Population(mutationRate, populationSize, sourceImg);
  
}


void draw() {
  background(0,0,255);
  image(sourceImg, 0, 0);
  
  population.selection();
  population.reproduction();
  population.calcFitness();
  
  if (population.finished()) {
    noLoop();
    println("finished");
  }
  displayInfo();  
}

void displayInfo() {
  
  //int[] pixels = new int[resultImg.width*resultImg.height];
  
  DNA best = population.getBest();
  for (int i = 0; i < best.genes.length; i++) {
    resultImg.pixels[i] = color(0, 0, best.genes[i].bright);   
    //println(red(resultImg.pixels[i])); 
  }
  resultImg.updatePixels();
  
  image(resultImg, sourceImg.width, 0, resultImg.width, resultImg.height);
  
  float maxFitness = population.getMaxFitness();
  
  float offsetX = 450;
  float lineHeight = 20;
  fill(0);
  textSize(14);
  float y = 20;
  text("Population size: "+population.getPopulationSize(), offsetX, y);
  y += lineHeight;
  text("Nr generations: "+population.getGenerations(), offsetX, y); 
  y += lineHeight;
  text("Max fitness: "+nf(maxFitness, 0, 4) + "%", offsetX, y);
  y += lineHeight;
  text("Average fitness: "+nf(population.getAverageFitness(), 0, 4) + "%", offsetX, y);
  
}