class DNA {
  int[] genes;
  float fitness;
  
  DNA() {
    int num_genes = 1; //1 gene want maar 1 nummer nodig
    genes = new int[num_genes];
    for (int i = 0; i < genes.length; i++) {
      genes[i] = (int)random(0,100);
    }
  }
  
  DNA(int[] newgenes) {
    genes = newgenes;
  }
  
  DNA crossover(DNA partner) {
    /*
    //POGING 1:
    int crossover = int(random(genes.length));
    for (int i = 0; i < genes.length; i++) {
      
      if (i > crossover) child[i] = genes[i];
      else               child[i] = partner.genes[i];
    }
    DNA newgenes = new DNA(child);
    return newgenes;
    */
    
    /*
    //POGING 2:
    int[] child = new int[genes.length];
    
    for (int i = 0; i < genes.length; i++) {
      child[i] = (genes[i] + partner.genes[i])/2;
    }
    DNA newgenes = new DNA(child);
    return newgenes;
    */
    
    
    //POGING 3:
    int[] child = new int[genes.length];
    
    for (int i = 0; i < genes.length; i++) {
      child[i] = (int)random(100);
    }
    DNA newgenes = new DNA(child);
    return newgenes;
  }
  
  void mutate(float m) {
    for (int i = 0; i < genes.length; i++) {
      if (random(1) < m) {
         genes[i] += random(-10, 10);
         //genes[i] = (int)random(100);
         //genes[i] = (int)map(genes[i], -genes[i], genes[i], 0, 100);
         //genes[i] = Math.abs(genes[i]);
      }
    }
  }
}