class Col {
  DNA dna;
  int bright;
  float fitness;
  
  Col(DNA dna_) {
    dna= dna_;
    bright = dna.genes[0];
    fitness = 0;
  }
  
  float fitness(int target) {
    //test 1:
    //float percent = (100/(float)target)*bright;
    //fitness = percent;
    
    //test 2:
    /*
    float diff;
    if (bright > target) {
      diff = bright - target;
    } else {
      diff = target - bright;
    }
    fitness = 100 - diff;
    */
    
    //test 3:
    fitness = Math.abs(target-bright);
    //fitness = pow(fitness, 2);
    fitness = map(fitness, 0, 100, 100, 0);
    //println("fitness", fitness);
    
    //fitness = pow(target-bright, 2);
    
    /*
    println("target", target);
    println("bright", bright);
    println("fitness", fitness);
    println("----------------------");
    */
    
    return fitness;
  }
  
  float getFitness() {
    return fitness;
  }
  
  DNA getDNA() {
    return dna;
  }
}