class Timer {
  boolean running = false;
  int startTime, stopTime = 0;
  
  void start() {
    startTime = millis();
    running = true;
  }
  
  void stop() {
    if (running) {
      stopTime = millis();
    }
    running = false;
  }
  
  int getElapsedTime() {
    int elapsed;
    if (running) {
      elapsed = (millis()-startTime);
    } else {
      elapsed = stopTime-startTime;
    }
    return elapsed;
  }
  
    float getSeconds() {
      return (getElapsedTime() / 1000.0);
    }
}