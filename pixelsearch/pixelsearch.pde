Population population;
float mutationRate;
int populationSize;
int target;
int rectSize = 80;
PFont f;
Timer timer;

void setup() {
  f = createFont("courier", 32, true);
  
  colorMode(HSB, 360, 100, 100);
  size(800,200);
  mutationRate = 0.0;
  populationSize = 2;
  target = (int)random(100);
  population = new Population(mutationRate, populationSize, target);
  timer = new Timer();
  timer.start();
  //frameRate(1);
  
  //println("target", target);
  /*
  int c = (int)random(100);
  Col col = new Col(c);
  col.fitness(target);
  */
}


void draw() {
  background(255);
  
  if (population.finished()) {
    target = (int)random(100);
    population = new Population(mutationRate, populationSize, target);
    delay(500);
    timer.start();
    //noLoop();
  }
  
  
  population.selection();
  population.reproduction();
  population.calcFitness();
  displayInfo(); 
}

void displayInfo() {
  fill(0);
  textFont(f);
  
  textSize(18);
  float xOffset = (rectSize*3);
  float yOffset = 20;
  float lineHeight = 15;
  
  float maxFitness = population.getMaxFitness();
  Col best = population.getBest();
  //println(best.getDNA().genes[0]);
  
  color targetCol = color(0,0,target);
  fill(targetCol);
  rect(0, 10, rectSize, rectSize);
  
  color evolutionCol = color(0, 0, best.dna.genes[0]);
  
  fill(evolutionCol);
  rect(rectSize, 10, rectSize, rectSize);
  
  fill(0);
  text("total population:        " + populationSize, xOffset, yOffset);
  yOffset += lineHeight;
  
  text("total generations:         " + population.getGenerations(), xOffset, yOffset);
  yOffset += lineHeight;
  
  text("max fitness:         " + nf(maxFitness, 0, 2) + "%", xOffset, yOffset);
  yOffset += lineHeight;
  
  text("average fitness:      " + nf(population.getAverageFitness(), 0, 2)+"%", xOffset, yOffset);
  
  yOffset += lineHeight;
  text("mutation rate:            " + int(mutationRate * 100) + "%", xOffset, yOffset);
  
  //float runtime = millis()/1000.0;
    
  yOffset += lineHeight;
  
  if (population.finished()) {
    timer.stop();    
  }
  
  float runtime = timer.getSeconds();
  
  if (population.finished()) {
    text("Finished in :         " + runtime + " seconds", xOffset, yOffset);
  } else {
    //float runtime = currentRuntimeMillis/1000.0;
    text("Runtime:         " + runtime + " seconds", xOffset, yOffset);       
  }
    
}