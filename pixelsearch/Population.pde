import java.util.List;
import java.util.HashSet;
import java.util.Collections;
import java.util.Comparator;

class Population {
  float mutationRate;
  Col[] population;
  ArrayList<Col> matingPool;
  int generations;
  int target;
  boolean finished;
  
  Population(float m, int num, int targetBrightness) {
    mutationRate = m;
    population = new Col[num];
    target = targetBrightness;
    finished = false;
    
    //println("target", target);
    
    matingPool = new ArrayList<Col>();
    generations = 0;
    
    for (int i = 0; i < population.length; i++) {
      population[i] = new Col(new DNA());
    }
    
    calcFitness();
  }
  
  void calcFitness() {
    for (int i = 0; i < population.length; i++) {
      population[i].fitness(target);
      //println(population[i].fitness);
    }
  }
  
  void selection() {
    calcFitness();
    
    matingPool.clear();
    
    float maxFitness = getMaxFitness();
     //<>//
    
    for (int i = 0; i < population.length; i++) { //<>//
      float fitnessNormal = map(population[i].getFitness(), 0, maxFitness, 0, 100);
      //float fitnessNormal = (100/maxFitness)*population[i].getFitness();
      //int n = (int) (fitnessNormal * 100);  // Arbitrary multiplier
      int n = (int)fitnessNormal;
      /*
      println("maxFitness", maxFitness);
      println("target", target);
      println("bright", population[i].getDNA().genes[0]);
      println("fitness", population[i].getFitness());
      println("fitnessNormal", fitnessNormal);
      println("n", n);
      println("---------------------");
      */
      for (int j = 0; j < n; j++) {
        matingPool.add(population[i]);
      }
    }
    
    List<Col> newList = new ArrayList<Col>(new HashSet<Col>(matingPool));
    Collections.sort(newList, new Comparator<Col>() 
    {
         @Override
         public int compare(Col lhs, Col rhs) {
    
           return Integer.valueOf(lhs.getDNA().genes[0]).compareTo(rhs.getDNA().genes[0]);
          }
     });
    
    String unique = "";
    for (Col item: newList) {
      unique += "," + item.getDNA().genes[0];
    }
    
    //println(unique);
    
    //println(matingPool.size());
  }
  
  // Making the next generation
  void reproduction() {
    // Refill the population with children from the mating pool
    for (int i = 0; i < population.length; i++) {
      // Sping the wheel of fortune to pick two parents
      
      int m = int(random(matingPool.size()));
      int d = int(random(matingPool.size()));
      
      // Pick two parents
      Col mom = matingPool.get(m);
      Col dad = matingPool.get(d);
      
      // Get their genes
      DNA momgenes = mom.getDNA();
      DNA dadgenes = dad.getDNA();
      
      // Mate their genes
      DNA childgenes = momgenes.crossover(dadgenes);
     
      // Mutate their genes
      childgenes.mutate(mutationRate);
      Col child = new Col(childgenes);
      if (child.fitness(target) < mom.fitness(target)) {
        child = mom;
      }
      // Fill the new population with the new child
      population[i] = child;
    }
    generations++;
    
  }
  
  // Find highest fintess for the population
  float getMaxFitness() {
    float record = 0;
    for (int i = 0; i < population.length; i++) {
      //println("fitnesss", population[i].fitness);
      if (population[i].fitness(target) > record) {
        record = population[i].getFitness();
      }
    }
    return record;
  }
  
  Col getBest() {
    float record = 0;
    int index = 0;
    for (int i = 0; i < population.length; i++) {
      //println("fitnesss", population[i].fitness);
      if (population[i].fitness > record) {
        record = population[i].fitness;
        index = i;
      }
    }
    
    if (population[index].dna.genes[0] == target) {
      finished = true;
    }
    return population[index];
  }
  
  boolean finished() {
    return finished;
  }
  
  int getGenerations() {
    return generations;
  }
  
  float getAverageFitness() {
    float total = 0;
    for (int i = 0; i < population.length; i++) {
      total += population[i].fitness;
    }
    return total / (population.length);
  }
}