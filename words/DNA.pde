class DNA {
  char[] genes = new char[18];
  String target = "to be or not to be";
  float fitness;
  
  DNA() {
    for (int i = 0; i < genes.length; i++) {
      //generate random population
      genes[i] = (char) random(32, 128);
    }
  }
  
  void fitness() {
    int score = 0;
    for (int i = 0; i < genes.length; i++) {
      if (genes[i] == target.charAt(i)) {
        score++;
      }
    }
    
    fitness = (float)score/target.length();
    
    if (score > 0) {   
      //println(score);
    }
  }
  
  DNA crossover(DNA partner) {
    /*
    bij het maken van een child, 
    moet je enkele genes van de parent1 samenvoegen met parent2
    */
    DNA child = new DNA();
    int midpoint = int(random(genes.length));
    
    for (int i = 0; i < genes.length; i++) {
      if (i > midpoint) {
        child.genes[i] = genes[i];
      } else {
        child.genes[i] = partner.genes[i];
      }
    }
    
    return child;
  }
  
  void mutate(float mutationRate) {
    for (int i = 0; i < genes.length; i++) {
      if (random(1) < mutationRate) {
        genes[i] = (char) random(32, 128);
      }
    }
  }
  
  String getPhrase() {
    return new String(genes);
  }
}