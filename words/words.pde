//processing
DNA[] population = new DNA[100];
float mutationRate = 0.01;
PFont f;

void setup() {
  size(800,200);
  for (int i = 0; i < population.length; i++) {
    population[i] = new DNA(); //initialize member of the population
  }
  
  f = createFont("Courier", 12, true);
}

void draw() {
  for (int i = 0; i < population.length; i++) {
    population[i].fitness();
  }

  ArrayList<DNA> matingPool = new ArrayList<DNA>();

  for (int i = 0; i < population.length; i++) {
    int n = int(population[i].fitness * 100);
    
    /*
    een letter met een fitness van 4, moet 4 keer in de matingpool
    gestoken worden zodat hij 40% kans heeft gekozen te worden
    (probability theory)
    */
    for (int j = 0; j < n; j++) {
      matingPool.add(population[i]);
    }
  }
  
  //reproduction:
  for (int i = 0; i < population.length; i++) {
    /*
    telkens 2 parents kiezen uit de matingpool om
    een nieuw child te maken  
    */
    int a = int(random(matingPool.size()));
    int b = int(random(matingPool.size()));
    DNA parentA = matingPool.get(a);
    DNA parentB = matingPool.get(b);
    
    /*
    child maken via crossover: deeltje van DNA van parent 1 
    en deeltje van DNA van parent 2,  samenvoegen
    */
    DNA child = parentA.crossover(parentB);
    
    /*
    mutate functie toepassen:
    om diversiteit in de pool te houden, is er een kleine kans
    dat er de genes van de child muteren (veranderen)
    (mutationRate is per gene)
    */
    child.mutate(mutationRate);
    population[i] = child;
  }
  
  background(255);
  fill(0);
  String everything = "";
  for (int i = 0; i < population.length; i++) {
    everything += population[i].getPhrase() + "     ";
  }
  
  textFont(f, 12);
  text(everything, 10, 10, width, height);
}