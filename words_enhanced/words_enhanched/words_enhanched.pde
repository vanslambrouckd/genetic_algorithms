PFont f;
Population population;
int populationSize = 50;
String target;
float mutationRate = 0.01;

void setup() {
  size(640,360);
  f = createFont("Courier", 32, true);
  target = "david vanslambrouck";
  //1. lege population maken
  population = new Population(target, populationSize, mutationRate);  
  
}

void draw() {
  //1: selection
  population.generateMatingPool();
  
  //create generation
  population.createGeneration();
  
  displayInfo();
  
  if (population.isFinished()) {
    //println(millis() / 1000.0);
    noLoop();
  }
}

float getRuntime() {
  return millis()/1000.0;
}

void displayInfo() {
  background(255);
  fill(0);
  
  textSize(24);
  text("Best prhase: ", 20, 30);
  
  textSize(40);
  text(population.getBest(), 20, 100);
  
  textSize(15);
  text("Population size: "+population.getPopulationSize(), 20, 160);
  text("Nr generations: "+population.getGenerations(), 20, 180); 
  text("Average fitness: "+nf(population.getAverageFitness(), 0, 2) + "%", 20, 200);
  text("Mutation rate: "+int(mutationRate*100) + "%", 20, 220);
  
  if (population.isFinished()) {
    text("Runtime: "+getRuntime() + " seconds", 20, 240); 
  }
  
  textSize(10);
  text("All phrases:\n" + population.allPhrases(), 500, 20);
}