class Population {
  DNA[] population;
  String target;
  ArrayList<DNA>matingPool;
  int generations;
  boolean finished;
  
  Population(String targ, int populationSize, float mutationRate) {
    population = new DNA[populationSize];
    target = targ;
    generations = 0;
    finished = false;
    
    //population opvullen met random dna objecten
    for (int i = 0; i < population.length; i++) {
      population[i] = new DNA(target.length());
    }
    
    matingPool = new ArrayList<DNA>();
  }
  
  void generateMatingPool() {
      matingPool.clear();
      
      float maxFitness = 0;
      
      for (int i = 0; i < population.length; i++) {
        float fitness = population[i].fitness(target);
        if (fitness > maxFitness) {
          maxFitness = fitness;
        }
      }
      
      for (int i = 0; i < population.length; i++) {
        /*
        fitness waardes normalizeren: 
        de fitnesswaarde mappen van 0 tot maxFitness naar 0 tot 1
        */
        float fitness = map(population[i].fitness(target), 0, maxFitness, 0, 1);
        
        /*
        items met hogere fitness moeten meer in de matingpool komen
        bvb item heeft fitness van 0.2 => omzetten naar procent = 20%
        dus 2 keer in de matingPool tov item met fitness van 0.1 (10% = 1 keer)
        */
        int percent = (int)fitness*100;
        for (int j = 0; j < percent; j++) {
          matingPool.add(population[i]);
        }
      }
  }
  
  void createGeneration() {
    for (int i = 0; i < population.length; i++) {
       int a = (int)random(matingPool.size());
       int b = (int)random(matingPool.size());
       DNA parentA = matingPool.get(a);
       DNA parentB = matingPool.get(b);
       
       //crossover: child maken
       DNA child = parentA.crossover(parentB);
      
       //mutation toepassen op child
       child.mutate(mutationRate);
       
       //child toevoegen aan de populatie
       population[i] = child;
    }
    generations++;
  }
  
  boolean isFinished() {
    return finished;
  }
  
  String getBest() {
    float bestFitness = 0;
    int indexBest = 0;
    
    //meest fitte member van de population ophalen
    for (int i = 0; i < population.length; i++) {
      float fitness = population[i].fitness(target);
      if (fitness > bestFitness) {
        bestFitness = fitness;
        indexBest = i;
      }
    }
    
    if (population[indexBest].getPhrase().equals(target)) {
      finished = true;
    }
    return population[indexBest].getPhrase();
  }
  
  float getAverageFitness() {
    float totalFitness = 0;
    for (int i = 0; i < population.length; i++) {
      float fitness = population[i].fitness(target);
      totalFitness += fitness;
    }
    
    float avg = totalFitness / population.length;
    return avg;
  }
  
  int getGenerations() {
    return generations;
  }
  
  int getPopulationSize() {
    return population.length;
  }
  
  String allPhrases() {
    String allPhrases = "";
    for (int i = 0; i < population.length; i++) {
      allPhrases += population[i].getPhrase() + "\n";
    }
    
    return allPhrases;
  }
}